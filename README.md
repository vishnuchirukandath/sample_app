# Beer Manager

This Django application is capable of fetching beer receipes and update into our DB and expose as API

## Tech

[Django](https://www.djangoproject.com/) \
[Poetry](https://python-poetry.org/) \
[pytest](https://pytest-django.readthedocs.io/en/latest/) \
[pyenv](https://github.com/pyenv/pyenv) \
[direnv](https://direnv.net/) \
[postgres](https://www.postgresql.org/)



## Installation

After setting up the virtual environement. set up environemnt variable into .env file, setup .envrc file

```bash
poetry install
```

```bash
pythong manage.py migrate
```

## Start API
```bash
pythong manage.py runserver
```

## Import Beer

```python
pythong manage.py import_beer
```
## Test

```python
pytest
```

## License
[MIT](https://choosealicense.com/licenses/mit/)

Credits
-------

[vishnuchirukandath@gmail.com](mailto:vishnuchirukandath@gmail.com)