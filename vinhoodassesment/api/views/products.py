from rest_framework.authentication import TokenAuthentication
from rest_framework.parsers import JSONParser, MultiPartParser
from rest_framework.permissions import IsAuthenticated
from rest_framework.viewsets import ModelViewSet

from vinhoodassesment.api.serializers.products import BeerSerializer
from vinhoodassesment.core.models import Beer


class BeerViewSet(ModelViewSet):
    permission_classes = (IsAuthenticated,)
    serializer_class = BeerSerializer
    http_method_names = ['get']
    parser_classes = [MultiPartParser, JSONParser]
    queryset = Beer.objects.all()
