from rest_framework.routers import SimpleRouter
from vinhoodassesment.api.views.products import BeerViewSet

router = SimpleRouter()
router.register(r'beer', BeerViewSet, basename='api-permissions')

urlpatterns = router.urls
