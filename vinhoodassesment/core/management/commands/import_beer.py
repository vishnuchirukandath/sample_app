from django.core.exceptions import ValidationError
from django.core.management.base import BaseCommand, CommandError
from vinhoodassesment.core.models import Beer
import requests


class Command(BaseCommand):
    help = 'Import beer from https://api.punkapi.com/'

    def handle(self, *args, **kwargs):
        fields = [field.name for field in Beer._meta.get_fields()]
        url = 'https://api.punkapi.com/v2/beers'
        response = requests.get(url)
        data = response.json()
        for beer in data:
            try:
                Beer.objects.update_or_create(**{k: beer[k] for k in fields})
            except ValidationError:
                raise CommandError('Incorrect Data')

        self.stdout.write(self.style.SUCCESS('Successfully imported %s beer records' % len(data)))
