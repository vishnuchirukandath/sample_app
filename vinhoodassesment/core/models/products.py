from django.contrib.postgres.fields import ArrayField
from django.db import models


class Beer(models.Model):
    id = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=50)
    tagline = models.TextField()
    first_brewed = models.CharField(max_length=50)
    description = models.TextField()
    image_url = models.TextField()
    abv = models.FloatField(null=True)
    ibu = models.IntegerField(null=True)
    target_fg = models.IntegerField()
    target_og = models.IntegerField()
    ebc = models.IntegerField(null=True)
    srm = models.IntegerField(null=True)
    ph = models.FloatField(null=True)
    attenuation_level = models.IntegerField()
    volume = models.JSONField()
    boil_volume = models.JSONField()
    food_pairing = ArrayField(models.CharField(max_length=100), null=True, blank=True)
    brewers_tips = models.TextField()
    contributed_by = models.TextField()

    def __str__(self):
        return self.name