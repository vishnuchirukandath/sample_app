from io import StringIO

import pytest
from django.core.management import call_command


@pytest.mark.django_db
def run_command(*args, **kwargs):
    out = StringIO()
    call_command(
        "import_beer",
        *args,
        stdout=out,
        stderr=StringIO(),
        **kwargs,
    )
    return out.getvalue()


@pytest.mark.django_db
class TestImportBeetCommand:

    def test_dry_run(self):
        out = run_command()
        assert "Successfully imported" in out
