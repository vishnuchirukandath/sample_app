from pytest_utils.api_client import Client


class DetailMixin:
    def detail_of(self, pk):
        return f'{self.url}{pk}/'


class TestBeerViewSet(DetailMixin):
    url = '/api/v1/beer/'

    def test_access_control_of_authorized_user(self, admin_user):
        client = Client(admin_user)
        res = client.get(self.url)

        assert res.status_code == 200

    def test_access_control_of_unauthorized_user(self):
        client = Client()
        res = client.get(self.url)

        assert res.status_code == 403

    def test_endpoint_returns_data(self):
        client = Client()
        res = client.get(self.url)
        data = res.json()
        assert data is not None

    def test_endpoint_returns_no_data_initially(self, admin_user):
        client = Client(admin_user)
        res = client.get(self.url)
        data = res.json()
        assert len(data) == 0

    def test_endpoint_returns_data(self, admin_user, beer_dummy):
        client = Client(admin_user)
        res = client.get(self.url)
        data = res.json()
        assert len(data) == 1

        assert any(x.get('id') == beer_dummy.id for x in data)



