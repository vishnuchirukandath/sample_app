import pytest
from vinhoodassesment.api.serializers.products import BeerSerializer


@pytest.mark.django_db
class TestProjectSerializer:
    def test_slot_groups(self, beer_dummy):
        data = BeerSerializer(beer_dummy).data
        assert data.get('name') is beer_dummy.name
