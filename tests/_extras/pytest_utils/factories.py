import factory.django

from vinhoodassesment.core.models import Beer



class BeerFactory(factory.django.DjangoModelFactory):
    id = factory.Sequence(lambda n: n)
    name = factory.Sequence(lambda n: 'test name %02d' % n)
    tagline = factory.Sequence(lambda n: 'test name Tagline %02d' % n)
    first_brewed = factory.Sequence(lambda n: 'test first_brewed %02d' % n)
    description = factory.Sequence(lambda n: 'test description %02d' % n)
    image_url = factory.Sequence(lambda n: 'test image_url %02d' % n)
    abv = factory.Sequence(lambda n: float(n))
    ibu = factory.Sequence(lambda n: n)
    target_fg = factory.Sequence(lambda n: n)
    target_og = factory.Sequence(lambda n: n)
    ebc = factory.Sequence(lambda n:  n)
    srm = factory.Sequence(lambda n:  n)
    ph = factory.Sequence(lambda n: float(n))
    attenuation_level = factory.Sequence(lambda n: n)
    volume = factory.Dict({
        'test': "sdcard",
        'test': "sdcard",
    })
    boil_volume = factory.Dict({
        'test': "sdasd",
        'testa': "sdasd",
    })
    food_pairing = factory.List([
        'test' for _ in range(5)
    ])
    brewers_tips = factory.Sequence(lambda n: 'test brewers_tips %02d' % n)
    contributed_by = factory.Sequence(lambda n: 'test contributed_by %02d' % n)

    class Meta:
        model = Beer
