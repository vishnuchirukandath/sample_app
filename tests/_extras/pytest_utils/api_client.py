from typing import Dict, Optional

from rest_framework.authtoken.models import Token
from rest_framework.test import APIClient


class Client:
    def __init__(self, user=None, base_uri=''):
        self._client = APIClient(defaults={'format': 'json'})
        self._base_uri = base_uri
        if user:
            token, _ = Token.objects.get_or_create(user=user)
            self._client.credentials(HTTP_AUTHORIZATION=f'Token {token.key}')

    def _full_uri(self, suffix: str, query: Optional[Dict[str, str]] = None):
        if query is None:
            query = {}

        uri = f'{self._base_uri}{suffix}'
        if query:
            querystring = '&'.join(f'{k}={v}' for k, v in query.items())
            uri = f'{uri}?{querystring}'
        return uri

    def login(self, user):
        self._client.force_login(user)

    def get(self, relative_uri, query=None, *args, **kwargs):
        uri = self._full_uri(relative_uri, query)
        return self._client.get(uri, *args, **kwargs)

    def post(self, relative_uri, *args, **kwargs):
        uri = self._full_uri(relative_uri)
        return self._client.post(uri, *args, **kwargs)

    def put(self, relative_uri, *args, **kwargs):
        uri = self._full_uri(relative_uri)
        return self._client.put(uri, *args, **kwargs)

    def patch(self, relative_uri, *args, **kwargs):
        uri = self._full_uri(relative_uri)
        return self._client.patch(uri, *args, **kwargs)

    def delete(self, relative_uri, *args, **kwargs):
        uri = self._full_uri(relative_uri)
        return self._client.delete(uri, *args, **kwargs)
