import sys
from pathlib import Path

import pytest


def pytest_configure(config):
    here = Path(__file__).parent
    sys.path.insert(0, str(here / '_extras'))

    import django
    django.setup()


@pytest.fixture()
def beer_dummy(db):
    from pytest_utils.factories import BeerFactory
    return BeerFactory()